// Style for GroupScreenComponent

import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'white'
    },
})

export default styles