// Style for Clean History component

import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    main_container: {
        paddingTop: 6
    }
})

export default styles